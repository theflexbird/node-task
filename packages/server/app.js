require("dotenv").config();
const Hapi = require("@hapi/hapi");
const Inert = require("@hapi/inert");
const Vision = require("@hapi/vision");
const routes = require("./routes");
const { Database } = require("mongorito");
const models = require("./models");
const HapiSwagger = require("hapi-swagger");
const Pack = require("./package");

const database = new Database(
  `mongodb://${process.env.DB_HOST}:${process.env.DB_PORT}/${process.env.DB_NAME}`
);

database.connect();

database.register(Object.values(models));

const launch = async () => {
  const server = await new Hapi.Server({
    host: process.env.SERVER_HOST,
    port: process.env.SERVER_PORT
  });
  try {
    const swaggerOptions = {
      info: {
        title: "Test API Documentation",
        version: Pack.version
      }
    };

    await server.register([
      Inert,
      Vision,
      {
        plugin: HapiSwagger,
        options: swaggerOptions
      }
    ]);

    server.route(routes);
    await server.start();
  } catch (err) {
    process.exit(1);
  }
  console.log(`Server running at ${server.info.uri}`);
};

launch();
