const { get } = require("./cart/get");
const { addProduct } = require("./cart/addProduct");
const { create } = require("./cart/create");
const { removeProduct } = require("./cart/removeProduct");
const { checkout } = require("./cart/checkout");

module.exports = {
  get,
  addProduct,
  create,
  removeProduct,
  checkout
};
