const { cartModel } = require("../../models/cart");
const { productModel } = require("../../models/product");
const { ObjectId } = require("mongorito");

async function addProduct(cartId, product) {
  try {
    const cart = await cartModel.findOne({ _id: new ObjectId(cartId) });
    cart.set("products", [...cart.get("products"), new productModel(product)]);
    return cart.save();
  } catch (err) {
    return Promise.reject(err);
  }
}

module.exports = { addProduct };
