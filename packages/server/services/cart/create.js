const { cartModel } = require("../../models/cart");

async function create() {
  try {
    const cart = new cartModel({
      products: []
    });
    return cart.save();
  } catch (err) {
    return Promise.reject(err);
  }
}

module.exports = { create };
