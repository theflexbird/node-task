const { cartModel } = require("../../models/cart");
const { ObjectId } = require("mongorito");

async function get(id) {
  try {
    return await cartModel.findOne({ _id: new ObjectId(id) });
  } catch (err) {
    return Promise.reject(err);
  }
}

module.exports = { get };
