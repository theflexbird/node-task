const { cartModel } = require("../../models/cart");
const { ObjectId } = require("mongorito");
const { getCurrencyRates } = require("../../helpers/rates");

const calculatePriceOfCart = (products, currencies) => {
  return products
    .map(
      product =>
        (product.get("price") * product.get("quantity")) /
        currencies.rates[product.get("currency")]
    )
    .reduce((a, b) => a + b, 0)
    .toFixed(2);
};

async function checkout(id, currency) {
  try {
    const { data: currencies } = await getCurrencyRates(currency);
    const cart = await cartModel.findOne({ _id: new ObjectId(id) });
    const priceOfCart = calculatePriceOfCart(cart.get("products"), currencies);
    return { products: cart.get("products"), price: priceOfCart, currency };
  } catch (err) {
    return Promise.reject(err);
  }
}

module.exports = { checkout };
