const { cartModel } = require("../../models/cart");
const { ObjectId } = require("mongorito");

async function removeProduct(cartId, productId) {
  try {
    const cart = await cartModel.findOne({ _id: new ObjectId(cartId) });
    const leftProduct = cart
      .get("products")
      .filter(item => item.get("id") != productId);
    cart.unset("products");
    cart.set("products", [...leftProduct]);
    return cart.save();
  } catch (err) {
    return Promise.reject(err);
  }
}

module.exports = { removeProduct };
