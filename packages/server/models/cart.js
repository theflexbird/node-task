const { Model } = require("mongorito");
const { productModel } = require("./product");

class Cart extends Model {}

Cart.embeds("products", productModel);

module.exports = { cartModel: Cart };
