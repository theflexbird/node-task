const { cartModel } = require("./cart");
const { productModel } = require("./product");

module.exports = {
  cartModel,
  productModel
};
