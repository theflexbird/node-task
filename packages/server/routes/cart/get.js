const { get } = require("../../services");
const Joi = require("@hapi/joi");

module.exports = {
  method: "GET",
  path: "/api/cart/{id}",
  options: {
    validate: {
      params: Joi.object({
        id: Joi.string()
          .min(24)
          .max(24)
          .required()
          .description("the id of the cart")
      })
    }
  },
  handler: async ({ params }) => {
    const { id } = params;
    return get(id);
  }
};
