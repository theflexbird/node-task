const { removeProduct } = require("../../services");
const Joi = require("@hapi/joi");

module.exports = {
  method: "DELETE",
  path: "/api/cart/{id}/remove/{productId}",
  options: {
    description: "Delete product from cart",
    notes: "Remove product from the cart",
    tags: ["api"],
    validate: {
      params: Joi.object({
        id: Joi.string()
          .min(24)
          .max(24)
          .required()
          .description("the id of the cart"),
        productId: Joi.number()
          .integer()
          .required()
          .description("the id of the product")
      })
    }
  },
  handler: async ({ params }, h) => {
    const { id, productId } = params;
    try {
      return await removeProduct(id, productId);
    } catch (err) {
      return h.response(err).code(500);
    }
  }
};
