const { checkout } = require("../../services");
const Joi = require("@hapi/joi");
const acceptedCurrencies = require("../../acceptedCurrencies.json");

module.exports = {
  method: "GET",
  path: "/api/cart/{id}/checkout/{currency}",
  options: {
    description: "Checkout cart",
    notes:
      "Returns products in the cart with information about cost in selected currency",
    tags: ["api"],
    validate: {
      params: Joi.object({
        id: Joi.string()
          .min(24)
          .max(24)
          .required()
          .description("the id of the cart"),
        currency: Joi.string()
          .valid(...acceptedCurrencies)
          .required()
          .description(
            "the currency in which value of the cart will be calculated"
          )
      })
    }
  },
  handler: async ({ params }) => {
    const { id, currency } = params;
    return checkout(id, currency);
  }
};
