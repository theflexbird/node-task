const { addProduct } = require("../../services");
const Joi = require("@hapi/joi");
const acceptedCurrencies = require("../../acceptedCurrencies.json");

module.exports = {
  method: "PUT",
  path: "/api/cart/{id}/add",
  options: {
    description: "Add product to the cart",
    notes: "Adding product to the cart",
    tags: ["api"],
    validate: {
      params: Joi.object({
        id: Joi.string()
          .min(24)
          .max(24)
          .required()
          .description("the id of the cart")
      }),
      payload: Joi.object({
        product: Joi.object({
          id: Joi.number()
            .integer()
            .required()
            .description("the id of the product"),
          name: Joi.string()
            .required()
            .description("the name of the product"),
          price: Joi.number()
            .required()
            .description("the price of the product"),
          quantity: Joi.number()
            .integer()
            .required()
            .description("the quantity of the product"),
          currency: Joi.string()
            .valid(...acceptedCurrencies)
            .required()
            .description("the currency of the product"),
          description: Joi.string().description(
            "the description of the product"
          )
        })
      }).label("cart")
    }
  },
  handler: async ({ payload, params }, h) => {
    const { product } = payload;
    const { id } = params;
    try {
      return await addProduct(id, product);
    } catch (err) {
      return h.response(err).code(500);
    }
  }
};
