const { create } = require("../../services");

module.exports = {
  method: "POST",
  path: "/api/cart/create",
  options: {
    description: "Create cart",
    notes: "Creates the new cart",
    tags: ["api"]
  },
  handler: async (request, h) => {
    try {
      return await create();
    } catch (err) {
      return h.response(err).code(500);
    }
  }
};
