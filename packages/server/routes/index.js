const createCart = require("./cart/create");
const get = require("./cart/get");
const addProduct = require("./cart/addProduct");
const removeProduct = require("./cart/removeProduct");
const checkout = require("./cart/checkout");

module.exports = [createCart, get, addProduct, removeProduct, checkout];
