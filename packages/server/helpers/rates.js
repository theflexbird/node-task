const axios = require("axios");

const getCurrencyRates = async currency =>
  axios.get(`${process.env.CURRENCY_SERVICE_ADDRESS}/latest?base=${currency}`);

module.exports = { getCurrencyRates };
