# Node Task

## Installation

1. Set an `.env` file inside packages/server.
2. Simply run `npm install` in root level.

## Run

You can run it by using `Procfile` or by running `docker-compose`.
